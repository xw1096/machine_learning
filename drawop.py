import numpy as np
import cv2
from matplotlib import pyplot as plt

img = np.zeros((512, 512, 3), np.uint8)

# draw diagonal blue line with thickness 5
cv2.line(img, (0,0), (512, 512), (255, 0, 0), 5)

# draw rectangle
cv2.rectangle(img, (384, 0), (510, 128), (0, 255, 0), 3)

# draw circle
cv2.circle(img, (477, 63), 63, (0, 0, 255), -1)

# draw ellipse
cv2.ellipse(img, (256, 256), (100, 50), 0, 0, 180, 255, -1)

# draw polygon
pts = np.array([[10,5],[20,30],[70,20],[50,10]], np.int32)
pts = pts.reshape((-1,1,2))
cv2.polylines(img,[pts],True,(0,255,255))

# add text
font = cv2.FONT_HERSHEY_SIMPLEX
cv2.putText(img,'OpenCV',(10,500), font, 4,(255,255,255),2)

print(img)
cv2.imshow('image', img)

k = cv2.waitKey(0) & 0xFF
if k == 27:
    cv2.destroyALLWindows()
elif k == ord('s'):
    cv2.imwrite('draw.png', img)
    cv2.destroyALLWindows()

