import numpy as np
from sklearn.datasets import load_iris

data = load_iris()
features = data.data
features_names = data.feature_names
target = data.target
target_names = data.target_names
labels = target_names[target]
is_setosa = (labels == 'setosa')
features = features[~is_setosa]
labels = labels[~is_setosa]
is_virginica = (labels == 'virginica')

def fit_model(input, is_virginica):
    best_acc = -1.0
    for fi in range(input.shape[1]):
        thresh = input[:, fi]
        for t in thresh:
            input_i = input[:, fi]
            pred = (input_i > t)
            acc = (pred == is_virginica).mean()
            rev_acc = (pred == ~is_virginica).mean()
            if rev_acc > acc:
                reverse = True
                acc = rev_acc
            else:
                reverse = False
            if acc > best_acc:
                best_acc = acc
                best_fi = fi
                best_t = t
                best_reverse = reverse
    return best_fi, best_t, best_reverse

def predict(fi, t, reverse, example):
    test = (example[0, fi] > t)
    if reverse:
        test = not test
    return test

correct = 0.0
for ei in range(len(features)):
    training = np.ones(len(features), bool)
    training[ei] = False
    testing = ~training
    fi, t, reverse = fit_model(features[training], is_virginica[training])
    predictions = predict(fi, t, reverse, features[testing])
    correct += np.sum(predictions == is_virginica[testing])
acc = correct/float(len(features))
print(acc)
