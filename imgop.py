import numpy as np
import cv2
from matplotlib import pyplot as plt


img = cv2.imread('sample.jpg', 0)

"""
cv2.imshow('image', img)
k = cv2.waitKey(0) & 0xFF
if k == 27:
    cv2.destroyALLWindows()
elif k == ord('s'):
    cv2.imwrite('gray.png', img)
    cv2.destroyALLWindows()
"""

plt.imshow(img, cmap = 'gray', interpolation = 'bicubic')
plt.xticks([]), plt.yticks([])
plt.show()
