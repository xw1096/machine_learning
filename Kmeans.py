import sys
import os
import numpy as np
import scipy as sp
import nltk.stem
import sklearn.datasets
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.cluster import KMeans

english_stemmer = nltk.stem.SnowballStemmer('english')
class stemmedTfidfVectorizer(TfidfVectorizer):
    def build_analyzer(self):
        analyzer = super(TfidfVectorizer, self).build_analyzer()
        return lambda doc:(english_stemmer.stem(w) for w in analyzer(doc))


all_data = sklearn.datasets.fetch_20newsgroups(subset = 'all')
groups = ['comp.graphics', 'comp.os.ms-windows.misc', 'comp.sys.ibm.pc.hardware', 'comp.sys.mac.hardware', 'comp.windows.x', 'sci.space']
train_data = sklearn.datasets.fetch_20newsgroups(subset='train', categories=groups)
test_data = sklearn.datasets.fetch_20newsgroups(subset='test', categories=groups)
vectorizer = stemmedTfidfVectorizer(min_df = 10, max_df = 0.5, stop_words = 'english', decode_error = 'ignore')
vectorized = vectorizer.fit_transform(train_data.data)
num_clusters = 50
km = KMeans(n_clusters = num_clusters, init = 'random', n_init = 1, verbose = 1, random_state = 3)
km.fit(vectorized)
new_post = open('new_post.txt').read()
new_post_vec = vectorizer.transform([new_post])
new_post_label = km.predict(new_post_vec)[0]
similar_indices = (km.labels_ == new_post_label).nonzero()[0]
similar = []
for i in similar_indices:
    dist = sp.linalg.norm((new_post_vec - vectorized[i]).toarray())
    similar.append((dist, train_data.data[i]))
similar = sorted(similar)

print(len(similar))
print(similar[0])
