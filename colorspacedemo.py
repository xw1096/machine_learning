import cv2
import numpy as np

cap = cv2.VideoCapture(0)

while(1):
    _, frame = cap.read()
    hsv = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)
    
    # DEFINE RANGE OF COLOR
    lower_blue = np.array([110, 50,50])
    upper_blue = np.array([130, 255, 255])
    lower_red = np.array([-10, 100, 100])
    upper_red = np.array([10, 255, 255])
    lower_green = np.array([50, 100, 100])
    upper_green = np.array([70, 255, 255])

    msk_b = cv2.inRange(hsv, lower_blue, upper_blue)
    msk_r = cv2.inRange(hsv, lower_red, upper_red)
    msk_g = cv2.inRange(hsv, lower_green, upper_green)
    msk = msk_b + msk_g + msk_r    
    res = cv2.bitwise_and(frame, frame, mask = msk)
    
    cv2.imshow('frame', frame)
    cv2.imshow('mask', msk)
    cv2.imshow('res', res)
    k = cv2.waitKey(5) & 0xFF
    if k == 27:
        break

cv2.destroyAllWindows()
