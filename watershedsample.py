import cv2
import numpy as np

img = cv2.imread('coins.jpg')
gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

ret, thresh = cv2.threshold(gray, 0, 255, cv2.THRESH_BINARY_INV+cv2.THRESH_OTSU)
kernel = np.ones((3, 3), np.uint8)

opening = cv2.morphologyEx(thresh, cv2.MORPH_OPEN, kernel, iterations = 2)
sure_bg = cv2.dilate(opening, kernel, iterations = 3)
dist_transform = cv2.distanceTransform(opening, cv2.cv.CV_DIST_L2, 5)
ret, sure_fg = cv2.threshold(dist_transform, 0.7*dist_transform.max(), 255, 0)
sure_fg = np.uint8(sure_fg)
unknown = cv2.subtract(sure_bg, sure_fg)

# connectedcomponents

contours, hierarchy = cv2.findContours(sure_fg,cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)

i = 0
for cnt in contours:
    cv2.drawContours(sure_fg, [cnt], 0, i, -1)
    i = i+1

sure_fg = sure_fg + 1
sure_fg[unknown == 255] = 0
sure_fg = sure_fg.astype(np.int32)

test= cv2.watershed(img, sure_fg)

img[sure_fg == -1] = [0, 0, 255]

cv2.imwrite('watershed.jpg', img)
