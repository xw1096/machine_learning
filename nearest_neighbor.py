import numpy as np
import scipy as sp
from sklearn.neighbors import KNeighborsClassifier
from sklearn.cross_validation import KFold
from sklearn.pipeline import Pipeline
from sklearn.preprocessing import StandardScaler


classifier = KNeighborsClassifier(n_neighbors = 1)
classifier = Pipeline([('norm', StandardScaler()), ('knn', classifier)])
data = sp.genfromtxt("/home/wxt/Documents/machine_learning/seeds_dataset.txt")
features = data[:, 0:7]
labels = data[:, 7]
kf = KFold(len(features), n_folds = 5, shuffle = True)
means = [];

for training, testing in kf:
    classifier.fit(features[training], labels[training])
    prediction = classifier.predict(features[testing])
    curmean = np.mean(prediction == labels[testing])
    means.append(curmean)

print(np.mean(means))
